jQuery(document).ready(function($) {
    var radio = document.getElementById('radio_player');
    var play_btn = document.getElementById('radio_play');
    var play = $('#radio_play');
    var mute = $('#radio_btn');
    var volb = $('#radio_volume_level');
    var vol_btn = document.getElementById("radio_btn");
    var vol_level = document.getElementById("radio_volume_level");
    if (radio)
        radio.volume = volb.val();

    /*
   * //TODO maybe check if radio is playing and if yes,
   * then on change play station, otherwise let the user do it.
   */
    play.click(function() {
        if (radio.paused) {
            radio.play();
            // play.text("Pause");
            play_btn.classList.remove('playControl');
            play_btn.classList.add('playControl_playing');
        } else {
            radio.pause();
            play_btn.classList.remove('playControl_playing');
            play_btn.classList.add('playControl');
            // play.text("Lecture");
        }
    });

    mute.click(function() {
        var previousVol = null;
        vol_btn.classList.remove('volumeMuted');
        vol_btn.classList.remove('volumeLow');
        vol_btn.classList.remove('volumeMedium');
        vol_btn.classList.remove('volumeHigh');
        if (radio.muted) {
            radio.muted = false;
            vol_level.value = previousVol;
            setVolumeIcon();
        } else {
            radio.muted = true;
            vol_btn.classList.add('volumeMuted');
            previousVol = vol_level.value;
            vol_level.value = 0.0;
        }
    });

    volb.change(function() {
        radio.volume = volb.val();
        setVolumeIcon();
    });

    function setVolumeIcon() {
        if (vol_btn != null) {
            if (vol_btn.classList.contains('volumeMuted')) {
                vol_btn.classList.remove('volumeMuted');
            }
            if (vol_btn.classList.contains('volumeMedium')) {
                vol_btn.classList.remove('volumeMedium');
            }
            if (vol_btn.classList.contains('volumeHigh')) {
                vol_btn.classList.remove('volumeHigh');
            }
        }
        if (vol_level != null) {
            if (vol_level.value == 0.0) {
                // Sourd
                vol_btn.classList.add('volumeMuted');
            } else if ((vol_level.value >= 0.7)) {
                // Fort
                vol_btn.classList.add('volumeHigh');
            } else {
                // Moyen
                vol_btn.classList.add('volumeMedium');
            }
        }
    }

    function getXMLHttpRequest() {
        var xhr = null;

        if (window.XMLHttpRequest || window.ActiveXObject) {
            if (window.ActiveXObject) {
                try {
                    // Pour tout les navs sauf IE
                    xhr = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e) {
                    // Pour IE
                    xhr = new ActiveXObject("Microsoft.XMLHTTP");
                }
            } else {
                xhr = new XMLHttpRequest();
            }
        } else {
            alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
            return null;
        }
        return xhr;
    }

    function refreshTitle() {
        var xhr = getXMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && xhr.status == 200) {
                document.getElementById("currentTitle").innerHTML = xhr.responseText;
                setTimeout(refreshTitle, 2000);
            }
        }
        xhr.open("GET", "/wp-content/plugins/declic-radio-widget/rds/NowOnAir.txt", true);
        xhr.send(null);
    }

    setVolumeIcon()
    refreshTitle();
});
