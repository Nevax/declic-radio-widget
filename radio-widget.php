<?php
/*
    Plugin Name: Declic Radio widget
    Plugin URI: http://wordpress.org/plugins/declic-radio-widget/
    Contributors: Sudavar
    Author: Antoine GRAVELOT (Nevax)
    Description: Declic Radio Widget produces a widget allowing users listen to a radio station from your website.
    Version: 0.5.1
    Tags: radio widget, declic, radio, radio stations, radio player, audio element html5, widget
    Requires at least: 3.0.1
    Tested up to: 3.9
    Stable tag: 0.4.2
    Text Domain: declic-radio-widget
    License: GPLv2
    License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

/*
 * Adds Radio_widget widget.
 */
class Declic_Radio_widget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        $radio = array(
            'default' => 'http://live.francra.org:8000/DeclicRadio',
            'volume' => 7,
            'stations' => array(
                1 => array( 'name' => "Declic Radio", 'url' => "http://live.francra.org:8000/DeclicRadio")
              ),
            'auto' => 1
        );
        $radio = maybe_serialize ( $radio );
        add_option( 'declic_radio_settings', $radio );
        parent::__construct(
            'declic_radio_widget', // Base ID
            __( 'Declic Radio Widget' ), // Name
            array( 'description' => __( 'Lecteur html5 Declic radio', 'Utilise un élément html5 pour jouer la radio' ),
            ) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        $radio = get_option( 'declic_radio_settings' );
        $radio = maybe_unserialize( $radio );
        $title = apply_filters( 'widget_title', $instance['title'] );

        ?>
        <div class="radio-widget">
            <div class="radio_block">
                <audio  src="<?php echo $instance[ 'default' ]; ?>" id="radio_player"
                        <?php echo ( $instance[ 'auto' ] ) ? 'autoplay="autoplay"': ''; ?> >
                </audio>
                <p class="site-description"><b>En direct :</b></p>
                    <div id="radio_txt">
                        <div id="currentTitle">Inconnu</div>
                    </div>
                <div id="radio_controls">
                    <div id="radio_player">
                        <button type="button" id="radio_play" class="w3-button w3-circle w3-red playControl playBtn"></button>
                    </div>
                    <div id="bottom_player">
                        <div id="radio_volume">
                            <button type="button" id="radio_btn" class="radio_mute w3-button w3-circle w3-red volumeHigh volumeBtn"></button>
                            <input type="range" id="radio_volume_level" min="0" max="1" step="0.01" value="<?php echo $instance[ 'volume' ]; ?>"> </input>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        echo $args['after_widget'];
    }

    /**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
    public function form( $instance ) {
        $radio = get_option( 'declic_radio_settings' );
        $radio = maybe_unserialize( $radio );
        $instance = wp_parse_args( (array) $instance, $radio );
		if ( !isset( $instance[ 'title' ] ) )
			$instance[ 'title' ] = "";
		if ( !isset( $instance[ 'auto' ] ) )
			$instance[ 'auto' ] = 0;
        ?>
        <p><fieldset class="basic-grey">
            <legend><?php echo __( 'Settings' ); ?>:</legend>
            <label>
                <span><?php echo __( 'Title' ); ?></span>
                <input  id="<?php echo $this->get_field_id( 'title' ); ?>"
                        name="<?php echo $this->get_field_name('title'); ?>" type="text"
                        value="<?php esc_attr_e($instance[ 'title' ]); ?>" />
            </label>
            <label>
                <span><?php echo __( 'Default' ); ?></span>
                <select id="<?php echo $this->get_field_id( 'default' ); ?>"
                        name="<?php echo $this->get_field_name( 'default' ); ?>">
                    <?php
                        foreach( $instance[ 'stations' ] as $station ){
                            $line = "<option value=\"{$station[ 'url' ]}\"";
                            $line = ( $station[ 'url' ] == $instance[ 'default' ] ) ? $line . " selected=\"selected\">" : $line . ">";
                            $line = $line."{$station[ 'name' ]}</option>\n\t\t\t\t";
                            echo $line;
                        }
                    ?>
                </select>
            </label>
            <label>
                <span><?php echo __( 'Volume' ); ?></span>
                0.1<input type="range" min="0.1" max="1" step="0.01"
                        id="<?php echo $this->get_field_id( 'volume' ); ?>"
                        name="<?php echo $this->get_field_name( 'volume' ); ?>"
                        value="<?php echo $instance[ 'volume' ]; ?>">10
            </label>
            <label>
                <span><?php echo __( 'AutoPlay' ); ?></span>
                <input  type="checkbox" value="1" <?php if($instance[ 'auto' ]) echo 'checked="checked"'; ?>
                        id="<?php echo $this->get_field_id( 'auto' ); ?>"
                        name="<?php echo $this->get_field_name( 'auto' ); ?>"
                        />
            </label>
        </fieldset></p>
		<?php
	}

    /**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance[ 'title' ] = strip_tags($new_instance[ 'title' ]);
        $instance[ 'default' ] = strip_tags($new_instance[ 'default' ]);
        $instance[ 'volume' ] = strip_tags($new_instance[ 'volume' ]);
        $instance[ 'auto' ] = ($new_instance[ 'auto' ]) ? strip_tags($new_instance[ 'auto' ]) : 0;
        return $instance;
    }
}

require_once dirname( __FILE__ ) . '/radio-widget-settings.php';
if( is_admin() )
    $my_settings_page = new Declic_Radio_Widget_Settings();

// Function registering the radio widget
function declic_register_radio_widget() {
    register_widget( 'Declic_Radio_Widget' );
}
add_action( 'widgets_init', 'declic_register_radio_widget');

// Function registering radio widget scripts
function declic_register_radio_css_js() {
	wp_enqueue_script(
        'radio-script',
        plugins_url().'/declic-radio-widget/radio-js.js',
        array( 'jquery' ),
        false,
        true
	);
    wp_enqueue_style(
        'radio-style',
        plugins_url().'/declic-radio-widget/radio-style.css'
    );
}
add_action( 'wp_enqueue_scripts', 'declic_register_radio_css_js' );
add_action( 'widgets_init', 'declic_register_radio_css_js');

// Function removing radio widget options
function declic_remove_options() {
    delete_option( 'declic_radio_settings' );
}
register_deactivation_hook( __FILE__, 'declic_remove_options' );
