# Declic Radio Widget
Contributors: nevax  
Author URI: http://profiles.wordpress.org/nevax  
Plugin URI:  
Tags: radio widget, declic, radio, radio stations, radio player, audio element html5, widget  
Requires at least: 3.0.1  
Tested up to: 3.9  
Stable tag: 0.4.2  
Text Domain: declic-radio-widget  
License: GPLv2  
License URI: http://www.gnu.org/licenses/gpl-2.0.html  

Declic Radio Widget produces a widget allowing users listen to a radio station from your website.  

## Description

Declic Radio Widget produces a widget for your wordpress site allowing
your users listen to a radio station from your website.
It uses html5 audio element to play radio in sidebar.

## Features

* Admin can select default station, set initial volume and autoplay or not.
* Control of the radio stations available from an options page (Adding / Removing from db)

## Installation

This section describes how to install the plugin and get it working.

1. Upload `declic-radio-widget` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
4. Add the widget to your sidebar.

## Screenshots

1. Widget preview

## Changelog

= 0.4.2 =
* Some HTML bug fix

= 0.4 =
* Added control of radio stations available from an options page in admin menu
* 0.4.1 fixed wrong parent folder name

= 0.3.1 =
* Wrong function names changed

= 0.2 =
* Some minor changes

= 0.1 =
* First version
